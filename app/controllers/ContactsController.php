<?php
namespace App\Controllers;

use App\Models\Contact;
use \Notification;
use Input, Redirect, Sentry, Str, Mail;

class ContactsController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        return \View::make('admin.contacts.index')->with('contacts', Contact::all());
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        return \View::make('admin.contacts.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
		$contact = new contact;
    	$contact->name	        = Input::get('name');
        $contact->email	        = Input::get('email');
        $contact->investment	= Input::get('investment');
        $contact->message       = Input::get('message');

        $contact->save();

        Notification::success('Sua mensagem foi enviada com sucesso. Logo entraremos em contato! :)');

        $this->sendContactResponseEmail($contact);
        $this->sendContactNotificationEmail($contact);

        return Redirect::route('home')->with('returning-from-submit', 'true');
	}

    /**
     * Send a contact notification email to website admins
     *
     * @param  object  $contact
     */
    private function sendContactNotificationEmail($contact) {

        // the data that will be passed into the mail view blade template

        $data = array(
            'message_txt'=> $contact->message,
            'name' => $contact->name,
            'email' => $contact->email,
            'investment' => $contact->investment
        );

        // use Mail::send function to send email passing the data and using the $user variable in the closure
        Mail::send('site::email.contact.notification', $data, function($message) use ($contact)
        {
            $message->from('sevengo@sevengo.com.br', '7go');
            $message->to('contato@sevengo.com.br', 'Contato 7go')->subject('Notificação de contato');
        });

    }

    /**
     * Send a contact response email
     *
     * @param  object  $contact
     */
    private function sendContactResponseEmail($contact) {

        // the data that will be passed into the mail view blade template
        $data = array(
            'detail'=>'Your awesome detail here',
            'name' => $contact->name
        );

        // use Mail::send function to send email passing the data and using the $user variable in the closure
        Mail::send('site::email.contact.response', $data, function($message) use ($contact)
        {
            $message->from('contato@sevengo.com', '7go');
            $message->to($contact['email'], $contact['name'])->subject('Obrigado pelo interesse');
        });

    }



	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        return \View::make('admin.contacts.show')->with('contact', Contact::find($id));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        return \View::make('admin.contacts.edit')->with('contact', Contact::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		return 'Update';
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        return 'Destroy';
	}

}