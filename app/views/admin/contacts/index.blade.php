@extends('admin._layouts.default')
 
@section('main')
 
    <h1>
        contacts <a href="{{ URL::route('admin.contacts.create') }}" class="btn btn-success"><i class="icon-plus-sign"></i> Add new contact</a>
    </h1>
 
    <hr>
 
    <table class="table table-striped">
        <thead>
            <tr>
                <th>#</th>
                <th>Nome</th>
                <th>E-mail</th>
                <th>Mensagem</th>
                <th>Quantidade deseja investir</th>
                <th>When</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($contacts as $contact)
                <tr>
                    <td>{{ $contact->id }}</td>
                    <td>{{ $contact->name }}</td>
                    <td>{{ $contact->email }}</td>
                    <td>{{ $contact->message }}</td>
                    <td>{{ $contact->investment }}</td>
                    <td>{{ $contact->created_at }}</td>
                    <td>
                        <a href="{{ URL::route('admin.contacts.edit', $contact->id) }}" class="btn btn-success btn-mini pull-left">Edit</a>
 
                        {{ Form::open(array('route' => array('admin.contacts.destroy', $contact->id), 'method' => 'delete', 'data-confirm' => 'Are you sure?')) }}
                            <button type="submit" href="{{ URL::route('admin.contacts.destroy', $contact->id) }}" class="btn btn-danger btn-mini">Delete</button>
                        {{ Form::close() }}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
 
@stop