<?php
 
use App\Models\User;

 
class SentrySeeder extends Seeder {
 
    public function run()
    {
        DB::table('users')->delete();
        DB::table('groups')->delete();
        DB::table('users_groups')->delete();

        //Creating Admin user group
        Sentry::getGroupProvider()->create(array(
            'name'        => 'Admin',
            'permissions' => array('admin' => 1),
        ));
        $adminGroup = Sentry::getGroupProvider()->findByName('Admin');

        //Creating vincius user
        Sentry::getUserProvider()->create(array(
            'email'       => 'vinicius@sevengo.com.br',
            'password'    => "07gogo",
            'first_name'  => 'Vinicius',
            'last_name'   => 'Rodrigues',
            'activated'   => 1,
        ));
        // Assign user permissions
        $adminUser  = Sentry::getUserProvider()->findByLogin('vinicius@sevengo.com.br');
        $adminUser->addGroup($adminGroup);

        //Creating Marcus user
        Sentry::getUserProvider()->create(array(
            'email'       => 'marcus@sevengo.com.br',
            'password'    => "07gogo",
            'first_name'  => 'Marcus',
            'last_name'   => 'Silva',
            'activated'   => 1,
        ));
        // Assign user permissions
        $adminUser  = Sentry::getUserProvider()->findByLogin('marcus@sevengo.com.br');
        $adminUser->addGroup($adminGroup);
    }
 
}