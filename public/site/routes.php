<?php
 
Route::get('/', array('as' => 'home', function() {
    return View::make('site::index');
}));

Route::get('/faq', array('as' => 'faq', function() {
    return View::make('site::faq/faq');
}));

Route::resource('contacts', '\App\Controllers\ContactsController');