@extends('site::layouts.master')

    @section('content')
        @section('menu-itens')
           <!--  <li><a href="{{ URL::route('home') }}" id="back-home-btn">Home</a></li> -->
        @stop

        <div class="container">
            <div class="row">
                <div class="col-md-offset-1 col-md-10">
                    <h2 class="black">Perguntas frequentes</h2>
                </div>
            </div>

            <div class="row">
                <div class="col-md-offset-1 col-md-10">
                    <h3 class="red">Quanto custa?</h3>
                    <p>
                        R: Como o tempo de desenvolvimento do projeto é fixo, ou seja, o MVP final será entregue após 7 dias, o preço será fixado em
                        R$ 6.000,00 por projeto. Caso nós, juntos, cheguemos ao consenso de que o projeto demandará menos tempo para ser desenvolvido
                        o preço poderá ser renegociado.
                    </p>
                </div>
            </div>

            <div class="row">
                <div class="col-md-offset-1 col-md-10">
                    <h3 class="red">Como funciona o processo de desenvolvimento?</h3>
                    <p>R: A nossa metodologia segue as seguintes etapas:</p>
                    <ul>
                        <li>
                            <p><b>1º dia:</b>
                            Reservado para realizarmos nossa reunião via Skype. Nesta reunião, alinharemos as idéias
                            da nossa equipe com as suas, de modo que todos tenham uma visão ampla do projeto.</p>
                            <p>Nós dividiremos o projeto em um conjunto de funcionalidades que precisam ser desenvolvidas e,
                            a cada uma, nossa equipe atribuirá um valor, 2, 4, 8 ou 16, dependendo do nível de dificuldade. Juntos, escolheremos
                            quais funcionalidades serão incorporadas ao MVP, sendo que a soma de pontuação não poderá ultrapassar os
                            100 pontos. Queremos escolher apenas as funcionalidades que realmente são importantes para a validação
                            da sua idéia, poupando gastos desnecessários e resolvendo o problema de maneira inteligente e objetiva.</p>
                            <p>Ao fim da reunião teremos definido um escopo delimitidado do seu produto, que será transformado
                            no MVP.</p>
                        </li>
                        <li><p><b>2º ao 6º dias:</b>
                            Durante 5 dias nós estaremos trabalhando exclusivamente em seu projeto, transformando o que foi
                            decidido na reunião em um app funcional. Ao final de cada dia teremos uma reunião de 15 minutos em que
                            será apresentado o que já foi desenvolvido e colheremos seu feedback para continuarmos nos próximos dias.</p>
                        </li>
                        <li><p><b>7º dia:</b>
                            A manhã do último dia será ainda dedicada à realização dos últimos ajustes. Até as 14h da tarde estaremos
                            trabalhando naqueles retoques finais que são sempre importantes. Após as 14h realizaremos a reunião final, que será
                            destinada principalmente à apresentação do MVP.</p>
                            <p>Você verá, desenvolvidas, todas as funcionalidades que combinamos.</p>
                            <p>Após a sua apreciação de um trabalho bem feito (:D), teremos uma etapa de apresentação das ferramentas
                            de analise de números. Nós mostraremos, de forma breve, como utilizá-las para colher
                            feedback de seus usuários e como analisar esta informação para a validação da sua idéia.</p>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="row">
                <div class="col-md-offset-1 col-md-10">
                    <h3 class="red">Se, após o MVP, quisermos evoluir para uma versão mais completa, vocês fazem?</h3>
                    <p>R: Nós podemos evoluir o produto por meio de uma nova etapa 7go, que seguirá a mesma metodologia e terá os mesmos custos da primeira semana de desenvolvimento.</p>
                </div>
            </div>

            <div class="row">
                <div class="col-md-offset-1 col-md-10">
                    <h3 class="red">Porque apps nativos e não web apps?</h3>
                    <p>
                        R: Nosso foco é testar o mercado, certo? Certo. Apps nativos demandam mais tempo para serem desenvolvidos, certo? Certo.
                        Então é melhor desenvolvermos um Web App utilizando uma tecnologia como Phone Gap, certo? Errado!
                        Vamos explicar porque mostrando algumas vantagens de apps nativos:
                        <ul>
                            <li>Causam uma melhor primeira impressão.</li>
                            <li>São mais responsivos. Ambientes de desenvolvimento multiplataforma possuem um <i>overhead</i> no processo
                            de comunicação com o <i>hardware</i>, o que pode interferir na responsividade da interface.</li>
                            <li>São mais <i>user friendly</i>. A interface web não foi criada para dispositivos touch screens.</li>
                            <li>Novos <i>updates</i> das plataformas originais demoram para serem incorporados pelos ambientes multiplataforma, que
                            podem nem implementar todas as funcionalidades acrescentadas.</li>
                        </ul>
                        Nós sabemos do potencial da sua idéia e queremos lhe entregar um produto de alta qualidade que possa
                        ser utilizado ser transformado em um produto final.
                    </p>
                </div>
            </div>

            <div class="row">
                <div class="col-md-offset-1 col-md-10">
                    <h3 class="red">Experiência da equipe</h3>
                    <p>
                        R: A equipe 7go é composta principalmente por desenvolvedores mobile e designers
                        com experiências anteriores de mercado.
                        Todos os membros da equipe estiveram diretamente envolvidos no desenvolvimento
                        dos seguintes projetos, que estão disponíveis na Apple Store e/ou Google Play:

                        <ul>
                            <li>
                                COOPERFORTE -
                                <a href="https://itunes.apple.com/br/app/cooperforte/id558002561?mt=8" target="_blank">
                                    iPhone/iPad,
                                </a>
                                <a href="https://play.google.com/store/apps/details?id=br.com.isitecnologia.cooperforte&feature=search_result#?t=W251bGwsMSwxLDEsImJyLmNvbS5pc2l0ZWNub2xvZ2lhLmNvb3BlcmZvcnRlIl0" target="_blank">
                                    Android
                                </a>
                            </li>

                            <li><a href="https://itunes.apple.com/br/app/dietme/id437007576?mt=8&ign-mpt=uo%3D4" target="_blank">DIET ME - iPhone</a></li>
                            <li><a href="https://itunes.apple.com/br/app/praticas-sustentaveis-do-brasil/id581539135?mt=8&ign-mpt=uo%3D4" target="_blank">PRÁTICAS SUSTENTÁVEIS DO BRASIL - iPad</a></li>
                            <li><a href="https://itunes.apple.com/us/app/poke.it/id492226335?mt=8&ign-mpt=uo%3D4" target="_blank">POKE.IT - iPhone</a></li>
                        </ul>
                    </p>
                </div>
            </div>

        </div>

    @stop