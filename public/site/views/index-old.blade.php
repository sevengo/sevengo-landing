<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="">
		<meta name="author" content="">

		<title>7go</title>

		<!-- Favicon -->
		<link rel="shortcut icon" href="{{ asset('site/assets/img/speech_bubble-128.png') }}">

		<!-- Bootstrap core CSS -->
		<link href="{{ asset('site/assets/css/bootstrap.css') }}" rel="stylesheet">
		<link href="{{ asset('site/assets/css/style.css') }}" rel="stylesheet">
        <link href="{{ asset('site/assets/css/validation.css') }}" rel="stylesheet">

		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="../../assets/js/html5shiv.js"></script>
		<script src="../../assets/js/respond.min.js"></script>
		<![endif]-->

	</head>

	<body>

	<div class="navbar navbar-blue navbar-fixed-top">
		<div class="container">
			<span class="pull-left">
				<img alt="Logo" class="logo" src="{{ asset('site/assets/img/7go-logo-branca-flat-100x70.png') }}" id="logo-header">
			</span>	

			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navHeaderCollapse">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>

			<div class="collapse navbar-collapse navHeaderCollapse">
				<ul class="nav navbar-nav navbar-main navbar-right">
					<li><a id="servicos-btn" href="#">Serviços</a></li>
					<li><a href="#" id="planos-btn">Planos</a></li>
					<li class="active" id="fale-conosco-btn" ><a href="#">Quer saber mais? Fale com a gente!</a></li>
				</ul>
			</div>
		</div>
	</div>

	<div class="jumbotron jumbotron-header">
		<div class="container">
			<center>
			<!--<img alt="Logo" src="img/7go-logo-branca-flat-350x245.png" id="logo-header">-->
			
			<h2>Seu site, feito pra você</h2>
			<p>A melhor solução. As melhores pessoas. O melhor para o seu negócio.</p>
			<p>Quer saber mais?</p>
			
			<a href="#" class="btn btn-white btn-lg" style="margin-top:10px">Fale com a gente!</a>
			</center>
		</div>
	</div>



	<div class="container" id="servicos-div">
		<div class="row">
			<div class="col-md-3">
				<center>
					<!-- <span class="glyphicon glyphicon-briefcase glyphicon-lg"></span> -->
					<img src="{{ asset('site/assets/img/briefcase-256.png') }}" alt="..." class="img-rounded">
				</center>

				<h3>O seu negócio é diferente</h3>
				<p>O seu negócio não é um negócio qualquer, seu site não deve ser um site qualquer. Na 7go nós criamos uma solução personalizada para a sua empresa, fornecemos todo o suporte  para a manutenção do seu site e fazemos todo o necessário para colocar seu empreendimento em destaque.</p>
			
			</div>
			<div class="col-md-3">
				<center>
					<!-- <span class="glyphicon glyphicon-exclamation-sign glyphicon-lg"></span> -->
					<img src="{{ asset('site/assets/img/decision-256.png') }}" alt="..." class="img-rounded">
				</center>

				<h3>Não esquente com problemas técnicos</h3>
				<p>Todos os nossos planos incluem registro de domínio e hospedagem. Você não precisa esquentar com um problema que não é seu. Seu site é um meio de divulgação do seu
				negócio, não uma pedra no seu sapato.</p>
			</div>
			<div class="col-md-3">
				<center>
					<!-- <span class="glyphicon glyphicon-wrench glyphicon-lg"></span> -->
					<img src="{{ asset('site/assets/img/lifebuoy-256.png') }}" alt="..." class="img-rounded">
				</center>

				<h3>Um mês de suporte grátis</h3>
				<p>Para a sua comodidade, você terá direito a 2 chamados de técnicos no primeiro mês de contratação sem ter que pagar nada a mais por isso. É claro que sempre teremos uma equipe disponível online para ajudá-lo a resolver problemas e sanar dúvidas, mas é sempre bom ter um especialista diposto a ir até você.</p>
			</div>
			<div class="col-md-3">
				<center>
					<!-- <span class="glyphicon glyphicon-time glyphicon-lg"></span> -->
					<img src="{{ asset('site/assets/img/watch-256.png') }}" alt="..." class="img-rounded">

				</center>

				<h3>Seu site em 7 dias</h3>
				<p>Se você teve aquela idéia milhonária e precisa divulgá-la rapidamente, nós estamos aqui para ajudá-lo a fazer fortuna. Em apenas 7 dias você terá tudo o que você precisa para ver o seu negócio online em operação.</p>
			</div>
		</div>
	</div>


	<hr/>

	
		<div class="container">
			<center>
				<h2>Confira nossos planos</h2>
			</center>
			

			<div class="row" id="planos-div">
				<div class="col-md-10 col-lg-offset-1">

					<div class="col-md-4">
						<div class="panel panel-default panel-promotion">
							<div class="span3 tabela-planos-blue">
								<div class="tabela-planos-header-blue">
								<center><h3>Landing Page</h3></center>
								</div>
								<div class="tabela-planos-body">
								<p><b>Serviços básicos</b></p>
								<ul>
									<li>Registro de domínio</li>
									<li>Hospedagem</li>
									<li>Seu site em 7 dias (sujeito a disponibilidade)</li>
								</ul>

								<p><b>Serviços básicos</b></p>
								<ul>
									<li>Google AdWords</li>
									<li>Facebook Ads</li>
								</ul>
								
								<hr>
								</div>
								<center>
									<h4>A partir de:</h4>
									<p class="full-price">
										<span class="dollar">R$</span>120,00
									</p>
									<p class="divided-payment">
										mensais
									</p>
								</center>
								
							</div>
						</div>
					</div>

					<div class="col-md-4">
						<div class="panel panel-default panel-promotion">
							<div class="span3 tabela-planos-red">
								<div class="tabela-planos-header-red">
								<center><h3>Site Institucional</h3></center>
								</div>
								<div class="tabela-planos-body">
								<p><b>Serviços básicos</b></p>
								<ul>
									<li>Até 4 páginas</li>
									<li>Registro de domínio</li>
									<li>Hospedagem</li>
								</ul>

								<p><b>Serviços adicionais</b></p>
								<ul>
									<li>Seu site em 7 dias (sujeito a disponibilidade)</li>
									<li>Google AdWords</li>
									<li>Facebook Ads</li>
								</ul>								
								<hr>	
								</div>
								
								<center>
									<h4>A partir de:</h4>
									<p class="full-price">
										<span class="dollar">R$</span>300,00
									</p>	
									<p class="divided-payment">
										mensais
									</p>
								</center>
							</div>
						</div>
					</div>

					<div class="col-md-4">
						<div class="panel panel-default panel-promotion">
							<div class="span3 tabela-planos-blue">
								<div class="tabela-planos-header-blue">
								<center><h3>E-Commerce</h3></center>
								</div>
								<div class="tabela-planos-body">
								<p><b>Serviços básicos</b></p>
								<ul>
									<li>Registro de domínio</li>
									<li>Hospedagem</li>
								</ul>

								<p><b>Serviços adicionais</b></p>
								<ul>
									<li>Seu site em 7 dias (sujeito a disponibilidade)</li>
									<li>Google AdWords</li>
									<li>Facebook Ads</li>
								</ul>								
								<hr>
								</div>
								<center>
									<h4>A partir de:</h4>
									<p class="full-price">
										<span class="dollar">R$</span>450,00
									</p>
									<p class="divided-payment">
										mensais
									</p>
								</center>
							</div>
						</div>
						<!--<center><a href="#" class="btn btn-blue btn-lg" style="margin-top:10px">Contratar!</a></center>-->
					</div>

					
					

				</div>
			</div>
			<div class="row">
				<center>
					<p>Os preços estão sujeitos a alteração em função da quantidade de acessos ao site.</p>
				</center>
			</div>
		</div>
	
	<hr/>

	<div class="jumbotron" id="fale-conosco-div">
		<div class="container">
			<center>
			<h2>Fale com a gente</h2>

			<p>Ligue <span class="phone">(61) 9195-6395</span></p>
			<p>Deseja saber mais? Sugestões? Dúvidas?</p>

			{{ Notification::showAll() }}
			</center>


			    {{ Form::open( array(
			    		'class' => " form-horizontal col-lg-offset-3 ",
                        'id' => "contact-form",
                        'onsubmit' => "return contactValidation();",
			    		'route' => 'contacts.store')) }}
			        
			        <div class="form-group">
			            <div class="col-lg-8">
			                {{ Form::text('name', null,
			                	$attributes = array( 'class' => "form-control form-square required",
                                                'name' => "name",
			                			        'id' => "name" ,
			                			        'placeholder'=>"Nome*")) }}
			            </div>
			        </div>

                    <div class="form-group">
                        <div class="col-lg-8">
                            {{ Form::text('email', null,
                            $attributes = array( 'class' => "form-control form-square required",
                            'name' => "email",
                            'id' => "email" ,
                            'placeholder'=>"Email*")) }}
                        </div>
                    </div>

			        <div class="form-group">
			            <div class="col-lg-8">
			                {{ Form::text('phone', null,
			                	$attributes = array( 'class' => "form-control form-square required",
			                			        'type'=> "phone",
			                			        'id' => "phone" ,
			                			        'placeholder'=>"Telefone*")) }}
			            </div>
			        </div>

			        <div class="form-group">
			            <div class="col-lg-8">
			                {{ Form::textarea('message', null, 
			                	$attributes = array( 'class' => "form-control form-square",
			                			        'type'=> "message",
			                			        'id' => "message" ,
			                			        'rows' => "6" ,
			                			        'placeholder'=>"Mensagem")) }}
			            </div>
			        </div>
			 
			 
			        <div class="form-group">
			        <div class="col-lg-8">
				  <center>
			      		{{ Form::submit('Enviar', array('class' => 'btn btn-blue btn-lg')) }}
				  </center>
			        </div>
			        </div>
			 
			    {{ Form::close() }}
			<!--
			<form class="form-horizontal col-lg-offset-3" role="form">
			  <div class="form-group">
			    <div class="col-lg-8">
			      <input type="nome" class="form-control form-square" id="inputNome" placeholder="Nome*">
			    </div>
			  </div>
			  <div class="form-group">
			    <div class="col-lg-8">
			      <input type="telefone" class="form-control form-square" id="telefone" placeholder="Telefone*">
			    </div>
			  </div>
			  <div class="form-group">
			    <div class="col-lg-8">
			      <textarea type="mensagem" class="form-control form-square" rows="6" id="mensagem" placeholder="Mensagem"></textarea>
			    </div>
			  </div>

			  <div class="form-group">
			    <div class="col-lg-8">
				  <center>
			      <button type="submit" class="btn btn-blue btn-lg">Enviar</button>
				  </center>
			    </div>
			  </div>
			</form>
			-->
		</div>
	</div>

	<nav class="navbar navbar-default navbar-fixed-bottom navbar-bottom" role="navigation">
		<div class="container">
			<p class="navbar-text pull-left">Copyright © 7go 2013</p>
			<p class="navbar-text pull-right">Venha nos conhecer - CDT (Centro de Desenvolvimento Tecnológico) UnB, Sala 100/20</p>
			<p class="navbar-text pull-right">Liga pra gente! (61) 9195-6395</p>
		</div>
	</nav>


    <!-- Inclusão do Jquery -->
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" ></script>

    <!-- -->
	<script src="{{ asset('site/assets/js/bootstrap.js') }}"></script>

    <!-- Form Masks -->
    <script src="{{ asset('site/assets/js/jquery.inputmask.js') }}"></script>

	<script type="text/javascript">
		$(document).ready(function() {
			$("#servicos-btn").on("click", function( e ) {
				$("body").scrollTop($("#servicos-div").offset().top - 90);
				return false;
			});
			$("#planos-btn").on("click", function( e ) {
				$("body").scrollTop($("#planos-div").offset().top - 160);
				return false;
			});
			$("#fale-conosco-btn").on("click", function( e ) {
				$("body").scrollTop($("#fale-conosco-div").offset().top - 90);
				return false;
			});
            $("#phone").inputmask("mask", {"mask": "(99) 9999-9999"}); //specifying fn & options

            returning = false;
            <?php
                $value = Session::get('returning-from-submit');
                if($value) {
                    echo "returning = true;";
            }?>

            if (returning) {
                $("body").scrollTop($("#fale-conosco-div").offset().top - 160);
            }

		});

	</script>

    <script>
        function contactValidation() {

            validated = true;

            $(".required").each(function(index) {
                if (!$(this).val()) {
                    if ($(this).hasClass("required")) {
                        placeholder = $(this).attr("placeholder");
                        placeholder = placeholder.substring(0, placeholder.indexOf('*')+1);
                        $(this).attr("placeholder", placeholder+" (Por favor, preencha este campo)");
                        $(this).addClass("focus");
                    }
                    validated = false;
                }
                else {
                    $(this).removeClass("focus");
                }
            });

            return validated;
        }
    </script>

  </body>
</html>
