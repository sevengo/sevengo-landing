@extends('site::layouts.master')


    @section('content')

        @section('menu-itens')
            <li><a href="#" id="como-funciona-btn">Quero testar minha idéia!</a></li>
            <li><a href="#" id="como-trabalhamos-btn">Como trabalhamos?</a></li>
            <li class="active fale-conosco-btn" ><a href="#">Fale com a gente!</a></li>
            <!-- <li><a href="{{ URL::route('faq') }}">FAQ</a></li> -->

        @stop

        <div class="jumbotron jumbotron-header">
            <div class="container center">
                <img src="{{ asset('site/assets/img/iphone-128-white.png')}}">
                <img src="{{ asset('site/assets/img/android-128-white.png')}}">
                <!--
                <img src="{{ asset('site/assets/img/iphones.png')}}" width="30%" height="30%">
                -->
            </div>
            <div class="container">
                <center>
                    <h2>Da sua cabeça para o smartphone</h2>
                    <p>Entregamos seu MVP Mobile em 7 dias</p>

                    <a href="#" class="btn btn-background-color btn-lg fale-conosco-btn" style="margin-top:10px">Fale com a gente!</a>

                </center>
            </div>
        </div>

        <!--
        <div class="icon-container"><div><i><img src="{{ asset('site/assets/img/go-button.png')}}"></i></div></div>
        -->

        <div class="jumbotron jumbotron-content">
            <div class="container" id="como-funciona-div">
                <div class="row center">
                    <h2>Ok, ok, quero testar minha idéia, mas como?</h2>
                </div>

                <div class="row center">
                    <div class="col-md-4">

                        <div class="container">
                            <img src="{{ asset('site/assets/img/solutions-220-white.png')}}">
                            <h3>Traga a sua idéia</h3>
                            <p class="white">
                                Você traz a sua idéia para a nossa equipe e nós discutiremos como vamos transformá-la em um app.
                                Nossos profissionais são experts em ajustar idéias ao mercado de modo a permitir os melhores testes
                                para sua validação.
                            </p>
                        </div>
                    </div>


                    <div class="col-md-4">

                        <div class="container">
                            <img src="{{ asset('site/assets/img/smartphone-tablet-220-white.png')}}">
                            <h3>Torne-a concreta</h3>
                            <p class="white">
                                Adivinhe só qual é a melhor forma de validar sua idéia? Isso mesmo, testando-a!
                                O MVP mobile nada mais é do que uma primeira versão do seu produto. Esta versão conterá um conjunto
                                restrito de funcionalidades que, ao mesmo tempo, resolvem o problema proposto e permitem uma
                                validação, avaliação e/ou apresentação da solução.
                            </p>
                        </div>
                    </div>


                    <div class="col-md-4">

                        <div class="container">
                            <img src="{{ asset('site/assets/img/statistics-220-white.png')}}">
                            <h3>Analise os números</h3>
                            <p class="white">
                                Você terá, à sua disposição, variadas métricas para analisar o desempenho do seu MVP, podendo
                                comparar os resultados fornecidos por usuários reais com os resultados esperados. Em breve você poderá
                                saber se aqueles milhões realmente irão parar no seu bolso.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="jumbotron">
            <div class="container" id="como-trabalhamos-div" style="vertical-align: middle">
                <div class="row white">
                    <div class="col-md-8">
                        <h2 class="red">Como trabalhamos?</h2>
                        <p class="black">
                        <h4 class="red">
                            Nossa expertise e metodologias irão ajudá-lo a definir claramente o seu produto e quais as funcionalidades
                            deseja aplicar ao seu MVP. Estamos sempre de olho nas novas tecnologias do mercado!
                        </h4>

                        <p><img src="{{ asset('site/assets/img/ok-32.png')}}">&nbsp Design Thinking</p>
                        <p><img src="{{ asset('site/assets/img/ok-32.png')}}">&nbsp Flat UI</p>
                        <p><img src="{{ asset('site/assets/img/ok-32.png')}}">&nbsp Desenvolvimento Ágil</p>
                        <p><img src="{{ asset('site/assets/img/ok-32.png')}}">&nbsp Lean Start Up</p>
                        <p><img src="{{ asset('site/assets/img/ok-32.png')}}">&nbsp Desenvolvimento com código nativo</p>

                        </p>
                    </div>
                    <div class="col-md-4">
                        <img src="{{ asset('site/assets/img/radio_tower-256.png')}}" style="vertical-align: middle">
                    </div>
                </div>
            </div>
        </div>


        <div class="jumbotron jumbotron-contact" id="fale-conosco-div">
            <div class="container center">

                <h2 class="red">Em breve!</h2>

                <h4>Ficou interessado? Deseja saber mais? Sugestões? Dúvidas?</h4>

                {{ Notification::showAll() }}

                {{ Form::open( array(
                'class' => " form-horizontal col-lg-offset-3 ",
                'id' => "contact-form",
                'onsubmit' => "return contactValidation();",
                'route' => 'contacts.store')) }}

                <div class="form-group">
                    <div class="col-lg-8">
                        {{ Form::text('name', null,
                        $attributes = array( 'class' => "form-control form-square required",
                        'name' => "name",
                        'id' => "name" ,
                        'placeholder'=>"Nome*")) }}
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-lg-8">
                        {{ Form::text('email', null,
                        $attributes = array( 'class' => "form-control form-square required",
                        'name' => "email",
                        'id' => "email" ,
                        'placeholder'=>"Email*")) }}
                    </div>
                </div>

                <!--
                <div class="form-group">
                    <div class="col-lg-8">
                        {{ Form::text('phone', null,
                        $attributes = array( 'class' => "form-control form-square required",
                        'type'=> "phone",
                        'id' => "phone" ,
                        'placeholder'=>"Telefone*")) }}
                    </div>
                </div>
                -->
                <div class="form-group">
                    <div class="col-lg-8">
                        {{ Form::select('investment',
                        array(
                            'Quanto pretende investir em sua idéia?',
                            'Até R$ 6.000,00',
                            'Até R$ 10.000,00',
                            'Até R$ 12.000,00'
                        ),
                        null,
                        $attributes = array (
                            'class' => "form-control form-square"
                        )
                        ); }}
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-lg-8">
                        {{ Form::textarea('message', null,
                        $attributes = array( 'class' => "form-control form-square",
                        'type'=> "message",
                        'id' => "message" ,
                        'rows' => "6" ,
                        'placeholder'=>"Mensagem")) }}
                    </div>
                </div>


                <div class="form-group">
                    <div class="col-lg-8">
                        <center>
                            {{ Form::submit('Enviar', array('class' => 'btn btn-background-color btn-lg')) }}
                        </center>
                    </div>
                </div>

                {{ Form::close() }}
            </div>
        </div>

    @stop


    @section('other-scripts')
        @parent

        <!-- Form Masks -->
        <script src="{{ asset('site/assets/js/jquery.inputmask.js') }}"></script>

        <script type="text/javascript">
            $(document).ready(function() {

                $(".back-to-top-btn").click(function() {
                    scrollToElement("body", 300);
                    return false;
                });

                $("#como-funciona-btn").click(function() {
                    scrollToElement("#como-funciona-div", 300, -160);
                    return false;
                });

                $("#como-trabalhamos-btn").click(function() {
                    scrollToElement("#como-trabalhamos-div", 300, -149);
                    return false;
                });

                $(".fale-conosco-btn").click(function() {
                    scrollToElement("#fale-conosco-div", 300, -115);
                    return false;
                });

                $("#phone").inputmask("mask", {"mask": "(99) 9999-9999"}); //specifying fn & options

                returning = false;
                <?php
                    $value = Session::get('returning-from-submit');
                    if($value) {
                        echo "returning = true;";
                }?>
                if (returning) {
                    $("body").scrollTop($("#fale-conosco-div").offset().top -115);
                }
            });
        </script>

        <script>
            function contactValidation() {
                validated = true;

                $(".required").each(function(index) {
                    if (!$(this).val()) {
                        if ($(this).hasClass("required")) {
                            placeholder = $(this).attr("placeholder");
                            placeholder = placeholder.substring(0, placeholder.indexOf('*')+1);
                            $(this).attr("placeholder", placeholder+" (Por favor, preencha este campo)");
                            $(this).addClass("focus");
                        }
                        validated = false;
                    }
                    else {
                        $(this).removeClass("focus");
                    }
                });
                return validated;
            }

            function scrollToElement(selector, time, verticalOffset) {
                time = typeof(time) != 'undefined' ? time : 1000;
                verticalOffset = typeof(verticalOffset) != 'undefined' ? verticalOffset : 0;
                element = $(selector);
                offset = element.offset();
                offsetTop = offset.top + verticalOffset;
                $('html, body').animate({
                    scrollTop: offsetTop
                }, time);
            }
        </script>
    @stop
