<!DOCTYPE html>

<html lang="en"
      xmlns="http://www.w3.org/1999/xhtml"
      xmlns:og="http://ogp.me/ns#"
      xmlns:fb="https://www.facebook.com/2008/fbml">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- Facebook meta tags -->
        <meta property="og:title" content="7go - Seu MVP em 7 dias"/>
        <meta property="og:description" content="Da sua cabeça para o smartphone! Valide suas idéias de apps de forma barata e eficiente."/>
        <meta property="og:image" content="{{ asset('site/assets/img/7go-fb-logo-300.png') }}"/>
        <meta property="og:url" content="http://www.sevengo.com.br/"/>
        <!-- End Facebook meta tags -->

        <!-- Facebook conversion pixel -->
        <script type="text/javascript">
            var fb_param = {};
            fb_param.pixel_id = '6010597422878';
            fb_param.value = '0.00';
            fb_param.currency = 'USD';
            (function(){
                var fpw = document.createElement('script');
                fpw.async = true;
                fpw.src = '//connect.facebook.net/en_US/fp.js';
                var ref = document.getElementsByTagName('script')[0];
                ref.parentNode.insertBefore(fpw, ref);
            })();
        </script>
        <noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/offsite_event.php?id=6010597422878&amp;value=0&amp;currency=USD" /></noscript>
        <!-- End Facebook conversion pixel -->


        <title>7go</title>

        <!-- Favicon -->
        <link rel="shortcut icon" href="{{ asset('site/assets/img/iphone-128.png') }}">

        <!-- Bootstrap core CSS -->
        <link href="{{ asset('site/assets/css/bootstrap.css') }}" rel="stylesheet">
        <link href="{{ asset('site/assets/css/style.css') }}" rel="stylesheet">
        <link href="{{ asset('site/assets/css/validation.css') }}" rel="stylesheet">
        @section('other-css')
        @show

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="../../assets/js/html5shiv.js"></script>
        <script src="../../assets/js/respond.min.js"></script>
        <![endif]-->

    </head>

    <body>

    <div class="navbar navbar-top navbar-fixed-top">
        <div class="container">
                <span class="pull-left">
                    <a href="{{ URL::route('home') }}" class="back-to-top-btn" id="home-btn">
                        <img alt="Logo" class="logo" src="{{ asset('site/assets/img/7go-logo-vermelho-branco.png') }}" id="logo-header">
                    </a>
                </span>

            <!--
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navHeaderCollapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button> -->

            <div class="collapse navbar-collapse navHeaderCollapse">
                <ul class="nav navbar-nav navbar-main navbar-right">
                    @yield('menu-itens')
                </ul>
            </div>
        </div>
    </div>


    <!-- Google Analytics -->
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-45141114-1', 'sevengo.com.br');
        ga('send', 'pageview');
    </script>

    <script type="text/javascript">
        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-45141114-1']);
        _gaq.push(['_trackPageview']);
        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;

            ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';

            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
    </script>
    <!-- End Google Analytics -->


    @yield('content')

    <!-- Facebook Social Button -->
    <div class="container">
        <a href="https://www.facebook.com/sevengo.br" target="_blank">
            <img alt="Facebook" class="social" src="{{ asset('site/assets/img/facebook.png') }}">
        </a>
    </div>
    <!-- End Facebook Social Button -->

    <div class="navbar navbar-default navbar-bottom" role="navigation">
        <div class="container">
            <p class="navbar-text">Copyright © 7go 2013</p>
            <p class="navbar-text pull-right">contato@sevengo.com.br</p>
        </div>
    </div>


    <!-- Inclusão do Jquery -->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" ></script>

    <!-- Bootstrap -->
    <script src="{{ asset('site/assets/js/bootstrap.js') }}"></script>

    @section('other-scripts')

    @show

    </body>
</html>
